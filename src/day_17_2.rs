use crate::day_17_1::calculate;
use crate::day_17_1::read_input;

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(6, 4, &input);
    println!("{}", result);
    Ok(())
}
