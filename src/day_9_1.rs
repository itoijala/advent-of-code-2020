use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

use crate::day_1_1;

pub fn calculate(input: &[usize]) -> Option<usize> {
    let mut v = Vec::with_capacity(25);
    for i in 25..input.len() - 1 {
        v.extend(&input[i - 25..i]);
        v.sort_unstable();
        if day_1_1::calculate(input[i], &v, 25).is_none() {
            return Some(input[i]);
        }
        v.clear();
    }
    None
}

pub fn read_input() -> anyhow::Result<Vec<usize>> {
    let f = File::open("input/9.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new)
                .and_then(|l| usize::from_str(&l).map_err(anyhow::Error::new))
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input).unwrap();
    println!("{}", result);
    Ok(())
}
