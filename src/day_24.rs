use std::collections::HashSet;
use std::iter;
use std::ops::Add;
use std::ops::AddAssign;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::all_consuming;
use nom::combinator::map;
use nom::combinator::value;
use nom::multi::many1;
use nom::sequence::terminated;
use nom::Finish;
use nom::IResult;

pub fn run() -> anyhow::Result<()> {
    let paths = read_input()?;
    let flipped_tiles = flip_tiles(&paths);
    let part_1 = part_1(&flipped_tiles);
    println!("{}", part_1);
    let part_2 = part_2(100, flipped_tiles);
    println!("{}", part_2);
    Ok(())
}

fn part_1(flipped_tiles: &HashSet<Tile>) -> usize {
    flipped_tiles.len()
}

fn part_2(rounds: u64, mut flipped_tiles: HashSet<Tile>) -> usize {
    for _ in 0..rounds {
        flipped_tiles = simulate_round(&flipped_tiles);
    }
    flipped_tiles.len()
}

fn flip_tiles(paths: &[Path]) -> HashSet<Tile> {
    let mut flipped = HashSet::new();
    for path in paths {
        let coordinates = path.destination();
        if flipped.contains(&coordinates) {
            flipped.remove(&coordinates);
        } else {
            flipped.insert(coordinates);
        }
    }
    flipped
}

fn simulate_round(flipped_tiles: &HashSet<Tile>) -> HashSet<Tile> {
    let mut new_tiles = HashSet::new();
    let mut visited = HashSet::new();
    for tile in flipped_tiles {
        for tile in iter::once(*tile).chain(tile.neighbours()) {
            if visited.contains(&tile) {
                continue;
            }
            let n = tile
                .neighbours()
                .filter(|t| flipped_tiles.contains(t))
                .count();
            let flipped = flipped_tiles.contains(&tile);
            if (flipped && n > 0 && n <= 2) || (!flipped && n == 2) {
                new_tiles.insert(tile);
            }
            visited.insert(tile);
        }
    }
    new_tiles
}

#[derive(Clone, Copy, Debug)]
enum Direction {
    East,
    West,
    NorthEast,
    NorthWest,
    SouthEast,
    SouthWest,
}

impl Direction {
    pub fn iter() -> impl Iterator<Item = Self> {
        [
            Self::East,
            Self::West,
            Self::NorthEast,
            Self::NorthWest,
            Self::SouthEast,
            Self::SouthWest,
        ]
        .iter()
        .copied()
    }

    pub fn delta(self) -> Tile {
        match self {
            Self::East => Tile(1, 0),
            Self::West => Tile(-1, 0),
            Self::NorthEast => Tile(0, 1),
            Self::NorthWest => Tile(-1, 1),
            Self::SouthEast => Tile(1, -1),
            Self::SouthWest => Tile(0, -1),
        }
    }
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Tile(i32, i32);

impl Tile {
    pub fn neighbours(self) -> impl Iterator<Item = Self> {
        Direction::iter().map(move |d| self + d)
    }
}

impl Add for Tile {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0, self.1 + other.1)
    }
}

impl AddAssign for Tile {
    fn add_assign(&mut self, other: Self) {
        *self = *self + other;
    }
}

impl Add<Direction> for Tile {
    type Output = Self;

    fn add(self, other: Direction) -> Self {
        self + other.delta()
    }
}

impl AddAssign<&Direction> for Tile {
    fn add_assign(&mut self, other: &Direction) {
        *self += other.delta();
    }
}

#[derive(Debug)]
struct Path(Vec<Direction>);

impl Path {
    pub fn destination(&self) -> Tile {
        let mut tile = Default::default();
        for direction in &self.0 {
            tile += direction;
        }
        tile
    }
}

fn read_input() -> anyhow::Result<Vec<Path>> {
    Ok(all_consuming(parse_paths)(include_str!("../input/24.txt"))
        .finish()?
        .1)
}

fn parse_paths(input: &str) -> IResult<&str, Vec<Path>> {
    many1(map(
        terminated(
            many1(alt((
                value(Direction::East, tag("e")),
                value(Direction::West, tag("w")),
                value(Direction::NorthEast, tag("ne")),
                value(Direction::NorthWest, tag("nw")),
                value(Direction::SouthEast, tag("se")),
                value(Direction::SouthWest, tag("sw")),
            ))),
            tag("\n"),
        ),
        Path,
    ))(input)
}
