use crate::day_12_1;
use crate::day_12_1::Op;
use crate::day_12_1::V2;

pub fn calculate(input: &[Op]) -> isize {
    let mut s = V2::new(0, 0);
    let mut w = V2::new(10, 1);
    for op in input {
        match op {
            Op::North(d) => w = w + V2::new(0, *d),
            Op::South(d) => w = w + V2::new(0, -d),
            Op::East(d) => w = w + V2::new(*d, 0),
            Op::West(d) => w = w + V2::new(-d, 0),
            Op::Left(d) => w = w.rotate_around(&s, *d),
            Op::Right(d) => w = w.rotate_around(&s, -d),
            Op::Forward(d) => {
                let r = w - s;
                s = s + (w - s) * *d;
                w = s + r;
            }
        }
        println!("{:?} {:?} {:?}", op, s, w);
    }
    s.manhattan()
}

pub fn run() -> anyhow::Result<()> {
    let input = day_12_1::read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
