use std::collections::HashMap;

use crate::day_16_1::read_input;
use crate::day_16_1::Data;

pub fn calculate(data: Data) -> u64 {
    let fields = data.fields;
    let yours = data.yours;
    let mut nearby = data.nearby;
    nearby.retain(|t| t.iter().all(|v| fields.is_valid(*v)));
    let fields = fields.0;

    let mut possibilities: Vec<(usize, Vec<usize>)> = Vec::new();
    for i in 0..fields.len() {
        possibilities.push((
            i,
            fields
                .iter()
                .enumerate()
                .filter(|(_, f)| nearby.iter().all(|n| f.is_valid(n[i])))
                .map(|(j, _)| j)
                .collect(),
        ));
    }
    let mut ordered_fields = HashMap::new();
    while !possibilities.is_empty() {
        let found = possibilities
            .iter()
            .enumerate()
            .filter(|(_, (_, ps))| ps.len() == 1)
            .map(|(i, _)| i)
            .next()
            .unwrap();

        let found = possibilities.remove(found);
        let fi = found.1[0];
        ordered_fields.insert(found.0, &fields[fi]);
        for (_, ps) in &mut possibilities {
            ps.retain(|i| *i != fi);
        }
    }

    ordered_fields
        .iter()
        .filter(|(_, v)| v.name.starts_with("departure"))
        .map(|(k, _)| yours[*k])
        .product()
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(input);
    println!("{}", result);
    Ok(())
}
