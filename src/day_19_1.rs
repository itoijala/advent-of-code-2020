use std::collections::HashMap;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::bytes::complete::take_until;
use nom::character::complete::anychar;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map_res;
use nom::multi::many1;
use nom::multi::separated_list1;
use nom::sequence::delimited;
use nom::sequence::terminated;
use nom::Finish;
use nom::IResult;

#[derive(Debug)]
pub enum Rule {
    Literal(char),
    Alternatives(Vec<Vec<u32>>),
}

pub type Rules = HashMap<u32, Rule>;

#[derive(Debug)]
pub struct Input {
    pub rules: Rules,
    pub strings: Vec<String>,
}

fn parse_inner<'r, 'i>(rules: &'r Rules, rule: &'r Rule, input: &'i str) -> Vec<&'i str> {
    if input.is_empty() {
        return vec![];
    }
    match rule {
        Rule::Literal(l) => {
            if input.chars().next().unwrap() == *l {
                vec![&input[1..]]
            } else {
                vec![]
            }
        }
        Rule::Alternatives(alts) => alts
            .iter()
            .map(|alt| {
                alt.iter().fold(vec![input], |inputs, seq| {
                    inputs
                        .iter()
                        .map(|i| parse_inner(rules, rules.get(seq).unwrap(), i))
                        .flatten()
                        .collect()
                })
            })
            .flatten()
            .collect(),
    }
}

fn parse(rules: &Rules, input: &str) -> bool {
    parse_inner(rules, rules.get(&0).unwrap(), input)
        .iter()
        .any(|s| s.is_empty())
}

pub fn calculate(input: &Input) -> usize {
    input
        .strings
        .iter()
        .filter(|s| parse(&input.rules, &s))
        .count()
}

fn parse_number(input: &str) -> IResult<&str, u32> {
    map_res(digit1, |d: &str| d.parse::<u32>())(input)
}

fn parse_literal(input: &str) -> IResult<&str, Rule> {
    let (input, l) = delimited(tag("\""), anychar, tag("\""))(input)?;
    Ok((input, Rule::Literal(l)))
}

fn parse_sequence(input: &str) -> IResult<&str, Rule> {
    let (input, s) = separated_list1(tag(" | "), separated_list1(tag(" "), parse_number))(input)?;
    Ok((input, Rule::Alternatives(s)))
}

fn parse_rule(input: &str) -> IResult<&str, (u32, Rule)> {
    let (input, id) = parse_number(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, rule) = alt((parse_literal, parse_sequence))(input)?;
    Ok((input, (id, rule)))
}

fn parse_input(input: &str) -> IResult<&str, Input> {
    let (input, rules) = many1(terminated(parse_rule, tag("\n")))(input)?;
    let rules = {
        let mut rs = HashMap::new();
        for (i, r) in rules {
            rs.insert(i, r);
        }
        rs
    };
    let (input, _) = tag("\n")(input)?;
    let (input, strings) = many1(terminated(take_until("\n"), tag("\n")))(input)?;
    let strings = strings.iter().map(|s| s.to_string()).collect();
    Ok((input, Input { rules, strings }))
}

pub fn read_input() -> anyhow::Result<Input> {
    const INPUT: &str = include_str!("../input/19.txt");
    let (_, input) = all_consuming(parse_input)(INPUT).finish()?;
    Ok(input)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
