use std::collections::HashMap;
use std::fs;

use nom::bytes::complete::tag;
use nom::bytes::complete::take_while1;
use nom::character::complete::one_of;
use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::Finish;
use nom::IResult;

pub fn validate(entry: &HashMap<String, String>) -> bool {
    for k in &["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"] {
        if !entry.contains_key::<str>(k) {
            return false;
        }
    }
    true
}

pub fn calculate(input: &Vec<HashMap<String, String>>) -> usize {
    input.iter().filter(|e| validate(*e)).count()
}

fn parse_key_value(input: &str) -> IResult<&str, &str> {
    take_while1(|c| c != ':' && c != ' ' && c != '\n')(input)
}

fn parse_pair(input: &str) -> IResult<&str, (&str, &str)> {
    separated_pair(parse_key_value, tag(":"), parse_key_value)(input)
}

fn parse_entry(input: &str) -> IResult<&str, HashMap<String, String>> {
    let (input, pairs) = separated_list1(one_of(" \n"), parse_pair)(input)?;
    let mut result = HashMap::new();
    for pair in pairs {
        result.insert(pair.0.to_string(), pair.1.to_string());
    }
    Ok((input, result))
}

fn parse_entries(input: &str) -> IResult<&str, Vec<HashMap<String, String>>> {
    separated_list1(tag("\n\n"), parse_entry)(input)
}

pub fn read_input() -> anyhow::Result<Vec<HashMap<String, String>>> {
    let file = fs::read_to_string("input/4.txt")?;
    let (_, entries) = parse_entries(&file)
        .finish()
        .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?;
    Ok(entries)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
