use anyhow::bail;

mod day_10_1;
mod day_10_2;
mod day_11_1;
mod day_11_2;
mod day_12_1;
mod day_12_2;
mod day_13_1;
mod day_13_2;
mod day_14_1;
mod day_14_2;
mod day_15_1;
mod day_15_2;
mod day_16_1;
mod day_16_2;
mod day_17_1;
mod day_17_2;
mod day_18_1;
mod day_18_2;
mod day_19_1;
mod day_19_2;
mod day_1_1;
mod day_1_2;
mod day_20;
mod day_21;
mod day_22;
mod day_23;
mod day_24;
mod day_25;
mod day_2_1;
mod day_2_2;
mod day_3_1;
mod day_3_2;
mod day_4_1;
mod day_4_2;
mod day_5_1;
mod day_5_2;
mod day_6_1;
mod day_6_2;
mod day_7_1;
mod day_7_2;
mod day_8_1;
mod day_8_2;
mod day_9_1;
mod day_9_2;

fn main() -> anyhow::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    match args[1].as_str() {
        "1.1" => day_1_1::run(),
        "1.2" => day_1_2::run(),
        "2.1" => day_2_1::run(),
        "2.2" => day_2_2::run(),
        "3.1" => day_3_1::run(),
        "3.2" => day_3_2::run(),
        "4.1" => day_4_1::run(),
        "4.2" => day_4_2::run(),
        "5.1" => day_5_1::run(),
        "5.2" => day_5_2::run(),
        "6.1" => day_6_1::run(),
        "6.2" => day_6_2::run(),
        "7.1" => day_7_1::run(),
        "7.2" => day_7_2::run(),
        "8.1" => day_8_1::run(),
        "8.2" => day_8_2::run(),
        "9.1" => day_9_1::run(),
        "9.2" => day_9_2::run(),
        "10.1" => day_10_1::run(),
        "10.2" => day_10_2::run(),
        "11.1" => day_11_1::run(),
        "11.2" => day_11_2::run(),
        "12.1" => day_12_1::run(),
        "12.2" => day_12_2::run(),
        "13.1" => day_13_1::run(),
        "13.2" => day_13_2::run(),
        "14.1" => day_14_1::run(),
        "14.2" => day_14_2::run(),
        "15.1" => day_15_1::run(),
        "15.2" => day_15_2::run(),
        "16.1" => day_16_1::run(),
        "16.2" => day_16_2::run(),
        "17.1" => day_17_1::run(),
        "17.2" => day_17_2::run(),
        "18.1" => day_18_1::run(),
        "18.2" => day_18_2::run(),
        "19.1" => day_19_1::run(),
        "19.2" => day_19_2::run(),
        "20" => day_20::run(),
        "21" => day_21::run(),
        "22" => day_22::run(),
        "23" => day_23::run(),
        "24" => day_24::run(),
        "25" => day_25::run(),
        _ => bail!("Unknown day: {}", args[1]),
    }
}
