use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::all_consuming;
use nom::combinator::value;
use nom::multi::many1;
use nom::sequence::terminated;
use nom::Finish;
use nom::IResult;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Point {
    Active,
    Inactive,
    Edge,
}

impl Point {
    pub fn active(&self) -> usize {
        match self {
            Point::Active => 1,
            Point::Inactive => 0,
            Point::Edge => 0,
        }
    }
}

pub fn calculate(cycles: usize, dimensions: usize, input: &[Vec<bool>]) -> usize {
    let lengths = {
        let mut lengths = vec![2 * (cycles + 1) + 1; dimensions];
        lengths[0] += input.len() - 1;
        lengths[1] += input[0].len() - 1;
        lengths
    };
    let index = |is: Vec<usize>| lengths.iter().zip(is.iter()).fold(0, |a, (d, i)| d * a + i);

    let mut array = vec![Point::Inactive; lengths.iter().product()];
    array
        .iter_mut()
        .enumerate()
        .filter(|(i, _)| {
            lengths
                .iter()
                .rev()
                .fold((false, *i), |(z, i), l| {
                    (z || (i % l == 0 || i % l == l - 1), i / l)
                })
                .0
        })
        .for_each(|(_, p)| *p = Point::Edge);

    for (x, is) in input.iter().enumerate() {
        for (y, i) in is.iter().enumerate() {
            let ix = index({
                let mut ix = vec![cycles + 1; dimensions];
                ix[0] = cycles + 1 + x;
                ix[1] = cycles + 1 + y;
                ix
            });
            array[ix] = if *i { Point::Active } else { Point::Inactive };
        }
    }

    let neighbours: Vec<isize> = (0..3isize.pow(dimensions as u32))
        .map(|i| {
            lengths
                .iter()
                .fold((0, i), |(a, i), l| (*l as isize * a + (i % 3 - 1), i / 3))
                .0
        })
        .filter(|i| *i != 0)
        .collect();

    for _ in 0..cycles {
        let old = array.clone();
        for (i, o) in old.iter().enumerate() {
            if *o == Point::Edge {
                continue;
            }
            let n: usize = neighbours
                .iter()
                .map(|n| old[(*n + i as isize) as usize].active())
                .sum();
            match o {
                Point::Active => {
                    if n < 2 || n > 3 {
                        array[i] = Point::Inactive;
                    }
                }
                Point::Inactive => {
                    if n == 3 {
                        array[i] = Point::Active;
                    }
                }
                Point::Edge => unreachable!(),
            }
        }
    }

    array.iter().map(|p| p.active()).sum()
}

fn parse_data(input: &str) -> IResult<&str, Vec<Vec<bool>>> {
    many1(terminated(
        many1(alt((value(false, tag(".")), value(true, tag("#"))))),
        tag("\n"),
    ))(input)
}

pub fn read_input() -> anyhow::Result<Vec<Vec<bool>>> {
    const INPUT: &str = include_str!("../input/17.txt");
    let (_, data) = all_consuming(parse_data)(INPUT).finish()?;
    Ok(data)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(6, 3, &input);
    println!("{}", result);
    Ok(())
}
