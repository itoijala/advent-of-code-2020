use std::collections::HashSet;
use std::collections::VecDeque;
use std::iter::FromIterator;

use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map_res;
use nom::multi::many1;
use nom::sequence::terminated;
use nom::Finish;
use nom::IResult;

pub fn run() -> anyhow::Result<()> {
    let game = read_input()?;
    let part_1 = part_1(game.clone());
    let part_2 = part_2(game);
    println!("{}", part_1);
    println!("{}", part_2);
    Ok(())
}

fn part_1(mut game: Game) -> usize {
    loop {
        if let Some(card_1) = game.player_1.pop_front() {
            if let Some(card_2) = game.player_2.pop_front() {
                if card_1 > card_2 {
                    game.player_1.push_back(card_1);
                    game.player_1.push_back(card_2);
                } else {
                    game.player_2.push_back(card_2);
                    game.player_2.push_back(card_1);
                }
            } else {
                game.player_1.push_front(card_1);
                break;
            }
        } else {
            break;
        }
    }
    points(&game)
}

fn part_2(game: Game) -> usize {
    let mut states = vec![State {
        game: game,
        cache: HashSet::new(),
        op: Op::Continue,
    }];
    let mut result = None;
    while let Some(state) = states.pop() {
        let State {
            mut game,
            mut cache,
            op,
        } = state;
        match op {
            Op::Continue => {
                if cache.contains(&game) {
                    result = Some((Player::Player1, game));
                    continue;
                }
                cache.insert(game.clone());
                if let Some(card_1) = game.player_1.pop_front() {
                    if let Some(card_2) = game.player_2.pop_front() {
                        if let Some(subgame) = game.copy_with_lengths(card_1, card_2) {
                            states.push(State {
                                game,
                                cache,
                                op: Op::Return(card_1, card_2),
                            });
                            states.push(State {
                                game: subgame,
                                cache: HashSet::new(),
                                op: Op::Continue,
                            });
                        } else {
                            if card_1 > card_2 {
                                game.player_1.push_back(card_1);
                                game.player_1.push_back(card_2);
                            } else {
                                game.player_2.push_back(card_2);
                                game.player_2.push_back(card_1);
                            }
                            states.push(State {
                                game,
                                cache,
                                op: Op::Continue,
                            });
                        }
                    } else {
                        game.player_1.push_front(card_1);
                        result = Some((Player::Player1, game));
                    }
                } else {
                    result = Some((Player::Player2, game));
                }
            }
            Op::Return(card_1, card_2) => {
                let result = result.take().unwrap();
                match result.0 {
                    Player::Player1 => {
                        game.player_1.push_back(card_1);
                        game.player_1.push_back(card_2);
                    }
                    Player::Player2 => {
                        game.player_2.push_back(card_2);
                        game.player_2.push_back(card_1);
                    }
                }
                states.push(State {
                    game,
                    cache,
                    op: Op::Continue,
                });
            }
        }
    }
    points(&result.unwrap().1)
}

fn points(game: &Game) -> usize {
    let winning_deck = if game.player_1.is_empty() {
        &game.player_2
    } else {
        &game.player_1
    };
    winning_deck
        .iter()
        .rev()
        .enumerate()
        .map(|(i, c)| (i + 1) * c)
        .sum()
}

type Card = usize;

type Deck = VecDeque<Card>;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Game {
    player_1: Deck,
    player_2: Deck,
}

impl Game {
    pub fn copy_with_lengths(&self, length_1: usize, length_2: usize) -> Option<Self> {
        if self.player_1.len() >= length_1 && self.player_2.len() >= length_2 {
            Some(Self {
                player_1: self.player_1.iter().copied().take(length_1).collect(),
                player_2: self.player_2.iter().copied().take(length_2).collect(),
            })
        } else {
            None
        }
    }
}

struct State {
    game: Game,
    cache: HashSet<Game>,
    op: Op,
}

#[derive(Debug)]
enum Op {
    Continue,
    Return(Card, Card),
}

#[derive(Clone, Copy, Debug)]
enum Player {
    Player1,
    Player2,
}

fn read_input() -> anyhow::Result<Game> {
    Ok(all_consuming(parse_game)(include_str!("../input/22.txt"))
        .finish()?
        .1)
}

fn parse_game(input: &str) -> IResult<&str, Game> {
    let (input, _) = tag("Player 1:\n")(input)?;
    let (input, player_1) = parse_deck(input)?;
    let (input, _) = tag("\nPlayer 2:\n")(input)?;
    let (input, player_2) = parse_deck(input)?;
    Ok((input, Game { player_1, player_2 }))
}

fn parse_deck(input: &str) -> IResult<&str, Deck> {
    let (input, deck) = many1(terminated(map_res(digit1, |d: &str| d.parse()), tag("\n")))(input)?;
    Ok((input, Deck::from_iter(deck.into_iter())))
}
