use crate::day_2_1;
use crate::day_2_1::Input;

pub fn check(input: &Input) -> bool {
    let first = input.password.chars().nth(input.low - 1).unwrap() == input.letter;
    let second = input.password.chars().nth(input.high - 1).unwrap() == input.letter;
    first ^ second
}

pub fn run() -> anyhow::Result<()> {
    let input = day_2_1::read_input()?;
    let result = day_2_1::calculate(check, &input);
    println!("{}", result);
    Ok(())
}
