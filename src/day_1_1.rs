use std::cmp::Ordering;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use anyhow::anyhow;

pub fn calculate(
    expected_sum: usize,
    input: &[usize],
    ignore_index: usize,
) -> Option<(usize, usize, usize)> {
    let mut low = 0;
    if low == ignore_index {
        low += 1;
    }
    let mut high = input.len() - 1;
    if high == ignore_index {
        high -= 1;
    }
    while low < high {
        let ilow = input[low];
        let ihigh = input[high];
        let sum = ilow + ihigh;
        match sum.cmp(&expected_sum) {
            Ordering::Equal => return Some((ilow, ihigh, ilow * ihigh)),
            Ordering::Less => {
                low += 1;
                if low == ignore_index {
                    low += 1;
                }
            }
            Ordering::Greater => {
                high -= 1;
                if high == ignore_index {
                    high -= 1;
                }
            }
        }
    }
    None
}

pub fn read_input() -> anyhow::Result<Vec<usize>> {
    let f = File::open("input/1.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new)
                .and_then(|s| s.parse::<usize>().map_err(anyhow::Error::new))
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let mut input = read_input()?;
    input.sort_unstable();
    let input = input;

    let result = calculate(2020, &input, input.len()).ok_or(anyhow!("No result found"))?;

    println!("{} x {} = {}", result.0, result.1, result.2);
    Ok(())
}
