use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn calculate(dx: usize, dy: usize, input: &Vec<Vec<bool>>) -> usize {
    let width = input[0].len();
    let mut trees = 0;
    let mut x = 0;
    let mut y = 0;
    while y < input.len() {
        if input[y][x] {
            trees += 1;
        }
        x = (x + dx) % width;
        y += dy;
    }
    trees
}

pub fn read_input() -> anyhow::Result<Vec<Vec<bool>>> {
    let f = File::open("input/3.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new)
                .and_then(|l| Ok(l.chars().map(|c| c == '#').collect::<Vec<_>>()))
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(3, 1, &input);
    println!("{}", result);
    Ok(())
}
