use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map_res;
use nom::combinator::value;
use nom::multi::separated_list1;
use nom::Finish;
use nom::IResult;

pub fn calculate(start: u64, periods: &[Option<u64>]) -> u64 {
    let (t, p) = periods
        .iter()
        .filter_map(|p| *p)
        .map(|p| ((start + p - 1) / p * p, p))
        .min()
        .unwrap();
    (t - start) * p
}

fn parse_line(input: &str) -> IResult<&str, Vec<Option<u64>>> {
    separated_list1(
        tag(","),
        alt((
            value(None, tag("x")),
            map_res(digit1, |n| Some(u64::from_str(n)).transpose()),
        )),
    )(input)
}

pub fn read_input() -> anyhow::Result<(u64, Vec<Option<u64>>)> {
    let f = File::open("input/13.txt")?;
    let reader = BufReader::new(f);
    let mut lines = reader.lines();
    let start = u64::from_str(&lines.next().unwrap()?).unwrap();
    let l = lines.next().unwrap()?;
    let periods = all_consuming(parse_line)(&l)
        .finish()
        .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?
        .1;
    Ok((start, periods))
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(input.0, &input.1);
    println!("{}", result);
    Ok(())
}
