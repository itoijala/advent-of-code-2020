use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::hash::Hash;
use std::io::BufRead;
use std::io::BufReader;
use std::iter::FromIterator;
use std::str::FromStr;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::alpha1;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::value;
use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::Finish;
use nom::IResult;

pub struct Node<N, E> {
    pub index: usize,
    pub name: N,
    pub followers: Vec<(usize, E)>,
}

pub struct Graph<N, E> {
    pub nodes: Vec<Node<N, E>>,
    names_to_indices: HashMap<N, usize>,
}

impl<N, E> Graph<N, E>
where
    N: Copy + Eq + Hash,
{
    pub fn new() -> Self {
        Self {
            nodes: Vec::new(),
            names_to_indices: HashMap::new(),
        }
    }

    pub fn get_or_add_node(&mut self, name: N) -> usize {
        let nodes = &mut self.nodes;
        *self.names_to_indices.entry(name).or_insert_with(|| {
            let index = nodes.len();
            nodes.push(Node {
                index,
                name,
                followers: Vec::new(),
            });
            index
        })
    }

    pub fn add_follower(&mut self, from: usize, to: usize, value: E) {
        self.nodes[from].followers.push((to, value));
    }
}

pub fn calculate(bag: &str, input: &[(String, Vec<(usize, String)>)]) -> usize {
    let mut graph = Graph::new();
    for (name, followers) in input {
        let i = graph.get_or_add_node(name.as_str());
        for (_, follower) in followers {
            let j = graph.get_or_add_node(follower.as_str());
            graph.add_follower(j, i, ());
        }
    }
    let bag_index = graph.get_or_add_node(bag);

    let mut found = HashSet::new();
    let mut stack = Vec::from_iter(graph.nodes[bag_index].followers.iter());
    while let Some(ni) = stack.pop() {
        found.insert(ni);
        for f in &graph.nodes[ni.0].followers {
            if !found.contains(f) {
                stack.push(f);
            }
        }
    }

    found.len()
}

fn parse_name(input: &str) -> IResult<&str, String> {
    let (input, (l, r)) = separated_pair(alpha1, tag(" "), alpha1)(input)?;
    Ok((input, format!("{} {}", l, r)))
}

fn parse_content(input: &str) -> IResult<&str, (usize, String)> {
    let (input, n) = digit1(input)?;
    let n = usize::from_str(n).unwrap();
    let (input, _) = tag(" ")(input)?;
    let (input, name) = parse_name(input)?;
    let mut input = input;
    if n == 1 {
        let (i, _) = tag(" bag")(input)?;
        input = i;
    } else {
        let (i, _) = tag(" bags")(input)?;
        input = i;
    }
    return Ok((input, (n, name)));
}

fn parse_line(input: &str) -> IResult<&str, (String, Vec<(usize, String)>)> {
    let (input, name) = parse_name(input)?;
    let (input, _) = tag(" bags contain ")(input)?;
    let (input, content) = alt((
        value(Vec::new(), tag("no other bags")),
        separated_list1(tag(", "), parse_content),
    ))(input)?;
    let (input, _) = tag(".")(input)?;
    Ok((input, (name, content)))
}

pub fn read_input() -> anyhow::Result<Vec<(String, Vec<(usize, String)>)>> {
    let f = File::open("input/7.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new).and_then(|l| {
                Ok(all_consuming(parse_line)(&l)
                    .finish()
                    .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?
                    .1)
            })
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate("shiny gold", &input);
    println!("{}", result);
    Ok(())
}
