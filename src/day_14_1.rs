use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map_res;
use nom::combinator::value;
use nom::multi::fold_many_m_n;
use nom::Finish;
use nom::IResult;

#[derive(Clone, Copy, Debug)]
pub struct Mask {
    pub mask: u64,
    pub value: u64,
}

#[derive(Clone, Copy, Debug)]
pub enum Op {
    Mask(Mask),
    Mem { address: u64, value: u64 },
}

pub fn calculate(input: &[Op]) -> u64 {
    let memory_size = input
        .iter()
        .filter_map(|o| match o {
            Op::Mask { .. } => None,
            Op::Mem { address: a, .. } => Some(a),
        })
        .max()
        .unwrap();
    let mut memory = vec![0; *memory_size as usize + 1];
    let mut mask = Mask { mask: 0, value: 0 };
    for op in input {
        match op {
            Op::Mask(m) => mask = *m,
            Op::Mem {
                address: a,
                value: v,
            } => memory[*a as usize] = v & !mask.mask | mask.value,
        }
    }
    memory.iter().sum()
}

fn parse_mask(input: &str) -> IResult<&str, Op> {
    let (input, _) = tag("mask = ")(input)?;
    let (input, (mask, value)) = fold_many_m_n(
        36,
        36,
        alt((
            value((1, 0), tag("0")),
            value((1, 1), tag("1")),
            value((0, 0), tag("X")),
        )),
        (0, 0),
        |(am, av), (nm, nv)| (2 * am + nm, 2 * av + nv),
    )(input)?;
    Ok((input, Op::Mask(Mask { mask, value })))
}

fn parse_number<T: FromStr>(input: &str) -> IResult<&str, T> {
    map_res(digit1, |n| T::from_str(n))(input)
}

fn parse_mem(input: &str) -> IResult<&str, Op> {
    let (input, _) = tag("mem[")(input)?;
    let (input, address) = parse_number(input)?;
    let (input, _) = tag("] = ")(input)?;
    let (input, value) = parse_number(input)?;
    Ok((input, Op::Mem { address, value }))
}

fn parse_line(input: &str) -> IResult<&str, Op> {
    alt((parse_mask, parse_mem))(input)
}

pub fn read_input() -> anyhow::Result<Vec<Op>> {
    let f = File::open("input/14.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new).and_then(|l| {
                Ok(all_consuming(parse_line)(&l)
                    .finish()
                    .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?
                    .1)
            })
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
