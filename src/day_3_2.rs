use crate::day_3_1;

pub fn run() -> anyhow::Result<()> {
    let input = day_3_1::read_input()?;
    let mut result = 1;
    for (dx, dy) in &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)] {
        result *= day_3_1::calculate(*dx, *dy, &input);
    }
    println!("{}", result);
    Ok(())
}
