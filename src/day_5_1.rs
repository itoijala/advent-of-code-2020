use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use anyhow::bail;

const LOG_ROWS: usize = 7;
const LOG_COLS: usize = 3;

#[derive(Debug)]
pub struct Pass {
    pub row: [bool; LOG_ROWS],
    pub col: [bool; LOG_COLS],
}

fn bsp_to_num(val: &[bool]) -> usize {
    val.iter()
        .fold(0, |acc, x| 2 * acc + if *x { 1 } else { 0 })
}

impl Pass {
    pub fn get_row(&self) -> usize {
        bsp_to_num(&self.row)
    }

    pub fn get_col(&self) -> usize {
        bsp_to_num(&self.col)
    }

    pub fn get_seat(&self) -> usize {
        8 * self.get_row() + self.get_col()
    }
}

pub fn calculate(input: &[Pass]) -> usize {
    input.iter().map(Pass::get_seat).max().unwrap()
}

pub fn read_input() -> anyhow::Result<Vec<Pass>> {
    let f = File::open("input/5.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new).and_then(|l| {
                if l.len() != LOG_ROWS + LOG_COLS {
                    bail!("Wrong line length");
                }
                let mut pass = Pass {
                    row: [false; LOG_ROWS],
                    col: [false; LOG_COLS],
                };
                for i in 0..LOG_ROWS {
                    pass.row[i] = l.chars().nth(i).unwrap() == 'B';
                }
                for i in 0..LOG_COLS {
                    pass.col[i] = l.chars().nth(LOG_ROWS + i).unwrap() == 'R';
                }
                Ok(pass)
            })
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
