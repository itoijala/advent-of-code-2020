use crate::day_13_1;

fn mod_inverse(a: u64, n: u64) -> Option<u64> {
    let mut t = (0, 1);
    let mut r = (n as i64, a as i64);
    while r.1 != 0 {
        let q = r.0 / r.1;
        t = (t.1, t.0 - q * t.1);
        r = (r.1, r.0 - q * r.1);
    }
    if r.0 > 1 {
        return None;
    }
    if t.0 < 0 {
        t.0 = t.0 + n as i64;
    }
    Some(t.0 as u64)
}

fn crt(congruences: &[(u64, u64)]) -> u64 {
    let nn: u64 = congruences.iter().map(|c| c.1).product();
    congruences
        .iter()
        .map(|(ak, nk)| {
            let nnk = nn / nk;
            let nnkp = mod_inverse(nnk, *nk).unwrap();
            ak * nnk * nnkp
        })
        .sum::<u64>()
        % nn
}

pub fn calculate(periods: &[Option<u64>]) -> u64 {
    let congruences = periods
        .iter()
        .enumerate()
        .filter_map(|(i, p)| p.map(|p| ((p - i as u64 % p) % p, p)))
        .collect::<Vec<_>>();
    crt(&congruences)
}

pub fn run() -> anyhow::Result<()> {
    let input = day_13_1::read_input()?;
    let result = calculate(&input.1);
    println!("{}", result);
    Ok(())
}
