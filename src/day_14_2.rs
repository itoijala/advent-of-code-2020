use std::collections::HashMap;

use crate::day_14_1::read_input;
use crate::day_14_1::Mask;
use crate::day_14_1::Op;

pub fn calculate(input: &[Op]) -> u64 {
    let mut memory = HashMap::new();
    let mut mask = Mask { mask: 0, value: 0 };
    for op in input {
        match op {
            Op::Mask(m) => mask = *m,
            Op::Mem {
                address: a,
                value: v,
            } => {
                let a = (*a & mask.mask) | (mask.mask & mask.value);
                let floatings = (0..36)
                    .filter_map(|i| {
                        if mask.mask & (1 << i) == 0 {
                            Some(i)
                        } else {
                            None
                        }
                    })
                    .collect::<Vec<_>>();
                for i in 0..(1 << floatings.len()) {
                    let fa: usize = floatings
                        .iter()
                        .enumerate()
                        .map(|(j, f)| ((i & (1 << j)) >> j) << f)
                        .sum();
                    memory.insert(a | fa as u64, *v);
                }
            }
        }
    }
    memory.values().sum()
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
