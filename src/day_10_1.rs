use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

pub fn calculate(mut input: Vec<usize>) -> usize {
    input.sort_unstable();
    let (one, three): (Vec<_>, _) = input
        .windows(2)
        .map(|w| w[1] - w[0])
        .filter(|d| *d != 2)
        .partition(|d| *d == 1);
    let one = one.len() + if input[0] == 1 { 1 } else { 0 };
    let three = three.len() + 1 + if input[0] == 3 { 1 } else { 0 };
    one * three
}

pub fn read_input() -> anyhow::Result<Vec<usize>> {
    let f = File::open("input/10.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new)
                .and_then(|l| usize::from_str(&l).map_err(anyhow::Error::new))
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(input);
    println!("{}", result);
    Ok(())
}
