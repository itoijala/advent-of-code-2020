use crate::day_15_1::calculate;
use crate::day_15_1::read_input;

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(30000000 - 1, &input);
    println!("{}", result);
    Ok(())
}
