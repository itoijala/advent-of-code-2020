use std::collections::HashMap;

use anyhow::anyhow;
use anyhow::bail;

#[derive(Clone, Copy, Debug)]
pub enum Op {
    Literal(u64),
    Binary(char),
}

pub type Expr = Vec<Op>;

pub fn calculate(operators: &Operators, exprs: &[Expr]) -> u64 {
    exprs
        .iter()
        .map(|expr| {
            let mut stack = Vec::new();
            for op in expr {
                match op {
                    Op::Literal(l) => stack.push(*l),
                    Op::Binary(o) => {
                        let l = stack.pop().unwrap();
                        let r = stack.pop().unwrap();
                        stack.push((operators.get(o).unwrap().eval)(l, r));
                    }
                }
            }
            assert!(stack.len() == 1);
            stack.pop().unwrap()
        })
        .sum()
}

pub struct Operator {
    pub precedence: u32,
    pub eval: Box<dyn Fn(u64, u64) -> u64>,
}

pub type Operators = HashMap<char, Operator>;

fn parse_line(operators: &Operators, line: &str) -> anyhow::Result<Expr> {
    let mut output = Vec::new();
    let mut stack = Vec::new();

    for c in line.chars() {
        if c.is_whitespace() {
            continue;
        }
        if c.is_ascii_digit() {
            output.push(Op::Literal(c as u64 - '0' as u64));
        } else if c == '(' {
            stack.push(c);
        } else if c == ')' {
            loop {
                let s = stack.pop().ok_or_else(|| anyhow!("Mismatched parens"))?;
                match s {
                    '(' => break,
                    s => {
                        output.push(Op::Binary(s));
                    }
                }
            }
        } else if let Some(o) = operators.get(&c) {
            while let Some(s) = stack.pop() {
                match s {
                    '(' => {
                        stack.push(s);
                        break;
                    }
                    s => {
                        if operators.get(&s).unwrap().precedence >= o.precedence {
                            output.push(Op::Binary(s));
                        } else {
                            stack.push(s);
                            break;
                        }
                    }
                }
            }
            stack.push(c);
        } else {
            bail!("Unknown char: '{}'", c);
        }
    }

    while let Some(s) = stack.pop() {
        output.push(Op::Binary(s));
    }

    Ok(output)
}

pub fn read_input(operators: &Operators) -> anyhow::Result<Vec<Expr>> {
    const INPUT: &str = include_str!("../input/18.txt");
    INPUT.lines().map(|l| parse_line(operators, l)).collect()
}

pub fn run() -> anyhow::Result<()> {
    let operators = &vec![
        (
            '+',
            Operator {
                precedence: 1,
                eval: Box::new(|l, r| l + r),
            },
        ),
        (
            '*',
            Operator {
                precedence: 1,
                eval: Box::new(|l, r| l * r),
            },
        ),
    ]
    .drain(..)
    .collect();
    let input = read_input(&operators)?;
    let result = calculate(&operators, &input);
    println!("{}", result);
    Ok(())
}
