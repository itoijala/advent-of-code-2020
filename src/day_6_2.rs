use std::collections::HashSet;
use std::iter::FromIterator;

use crate::day_6_1;

fn calculate_group(group: &Vec<String>) -> usize {
    let mut set: HashSet<char> = group[0].chars().collect();
    for m in group {
        let s = HashSet::from_iter(m.chars());
        set = set.intersection(&s).cloned().collect();
    }
    set.len()
}

pub fn calculate(input: &Vec<Vec<String>>) -> usize {
    input.iter().map(|g| calculate_group(g)).sum()
}

pub fn run() -> anyhow::Result<()> {
    let input = day_6_1::read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
