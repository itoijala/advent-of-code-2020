use std::fmt;
use std::fmt::Debug;
use std::mem;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map;
use nom::combinator::map_res;
use nom::combinator::value;
use nom::multi::many1;
use nom::sequence::delimited;
use nom::sequence::terminated;
use nom::Finish;
use nom::IResult;

pub fn run() -> anyhow::Result<()> {
    let tiles = read_input()?;
    let monster = read_monster()?;
    let matched_tiles = match_tiles(&tiles);
    let layout = lay_out_tiles(&matched_tiles);
    let image = stitch_image(&layout);
    let analysed = mark_monsters(&image, &monster);
    let part1 = calculate_part1(&layout);
    let part2 = calculate_part2(&analysed);
    println!("{}", part1);
    println!("{}", part2);
    Ok(())
}

fn calculate_part1(layout: &[Vec<TransformedTile>]) -> u64 {
    layout.first().unwrap().first().unwrap().tile.id
        * layout.first().unwrap().last().unwrap().tile.id
        * layout.last().unwrap().first().unwrap().tile.id
        * layout.last().unwrap().last().unwrap().tile.id
}

fn calculate_part2(image: &Pixels<Analysed<bool>>) -> usize {
    image
        .0
        .iter()
        .map(|r| {
            r.iter()
                .filter(|p| **p == Analysed::Undetected(true))
                .count()
        })
        .sum()
}

fn match_tiles(tiles: &[Tile]) -> Vec<MatchedTile> {
    let mut matched_tiles: Vec<MatchedTile> = tiles
        .iter()
        .map(|tile| MatchedTile {
            tile,
            edges: Edges::default(),
        })
        .collect();

    let mut rest = &mut matched_tiles[..];
    let mut index1 = 0;
    while let (i1, Some((tile1, r))) = (index1, rest.split_first_mut()) {
        rest = r;
        index1 = i1 + 1;
        for (i2, tile2) in rest.iter_mut().enumerate() {
            for edge1 in Edge::iter() {
                for edge2 in Edge::iter() {
                    for reversed in &[false, true] {
                        if {
                            if *reversed {
                                tile1.iter_edge(edge1).eq(tile2.iter_edge(edge2).rev())
                            } else {
                                tile1.iter_edge(edge1).eq(tile2.iter_edge(edge2))
                            }
                        } {
                            assert!(tile1.matching_edge_mut(edge1).is_none());
                            assert!(tile2.matching_edge_mut(edge2).is_none());
                            *tile1.matching_edge_mut(edge1) = Some(MatchingEdge {
                                tile: i1 + i2 + 1,
                                edge: edge2,
                                reversed: *reversed,
                            });
                            *tile2.matching_edge_mut(edge2) = Some(MatchingEdge {
                                tile: i1,
                                edge: edge1,
                                reversed: *reversed,
                            });
                        }
                    }
                }
            }
        }
    }

    matched_tiles
}

fn lay_out_tiles<'a>(matched_tiles: &'a [MatchedTile]) -> Vec<Vec<TransformedTile<'a>>> {
    let mut layout = Vec::new();
    let mut top_left: Option<&TransformedTile> = None;
    loop {
        let mut row = vec![if let Some(tl) = top_left {
            tl.below(&matched_tiles)
        } else {
            TransformedTile::top_left(&matched_tiles)
        }];
        let mut right = row.last().unwrap();
        while right.matching_edge(Edge::Right).is_some() {
            let tile = right.right(&matched_tiles);
            row.push(tile);
            right = row.last().unwrap();
        }
        layout.push(row);
        let left = layout.last().unwrap().first().unwrap();
        top_left = Some(left);
        if left.matching_edge(Edge::Bottom).is_none() {
            break;
        }
    }
    layout
}

fn stitch_image<'a>(layout: &'a [Vec<TransformedTile>]) -> Pixels<bool> {
    let without_edges: Vec<Vec<_>> = layout
        .iter()
        .map(|r| {
            r.iter()
                .map(|t| t.tile.pixels.transform(&t.transform).without_edges())
                .collect()
        })
        .collect();
    let mut image = Vec::new();
    for l in &without_edges {
        for r in 0..l[0].0.len() {
            let mut row = Vec::new();
            for c in l {
                row.extend(&c.0[r]);
            }
            image.push(row);
        }
    }
    Pixels(image)
}

fn mark_monsters(image: &Pixels<bool>, monster: &Pixels<Needle<bool>>) -> Pixels<Analysed<bool>> {
    let mut image = Pixels(
        image
            .0
            .iter()
            .map(|r| r.iter().map(|p| Analysed::Undetected(*p)).collect())
            .collect(),
    );
    let monster = &monster.0;
    'outer: for rotate_left in &[false, true] {
        for mirror_diagonal in &[false, true] {
            for mirror_anti_diagonal in &[false, true] {
                image = image.transform(&Transform {
                    rotate_left: *rotate_left,
                    mirror_diagonal: *mirror_diagonal,
                    mirror_anti_diagonal: *mirror_anti_diagonal,
                });
                let image = &mut image.0;

                let mut found_any = false;
                for iy in 0..image.len() - monster.len() {
                    for ix in 0..image[iy].len() - monster[0].len() {
                        let mut found = true;
                        for my in 0..monster.len() {
                            for mx in 0..monster[my].len() {
                                if let Needle::Search(n) = monster[my][mx] {
                                    found &= image[iy + my][ix + mx] == Analysed::Undetected(n);
                                }
                            }
                        }
                        if found {
                            found_any = true;
                            for my in 0..monster.len() {
                                for mx in 0..monster[my].len() {
                                    if let Needle::Search(n) = monster[my][mx] {
                                        image[iy + my][ix + mx] = Analysed::Detected(n);
                                    }
                                }
                            }
                        }
                    }
                }
                if found_any {
                    break 'outer;
                }
            }
        }
    }
    image
}

#[derive(Clone, Copy, Debug)]
enum Edge {
    Top,
    Left,
    Bottom,
    Right,
}

impl Edge {
    pub fn iter() -> impl DoubleEndedIterator<Item = Self> {
        [Self::Top, Self::Left, Self::Bottom, Self::Right]
            .iter()
            .copied()
    }
}

#[derive(Debug, Default)]
struct Transform {
    rotate_left: bool,
    mirror_diagonal: bool,
    mirror_anti_diagonal: bool,
}

#[derive(Clone, Copy, Debug)]
enum Needle<T> {
    Ignore,
    Search(T),
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Analysed<T> {
    Undetected(T),
    Detected(T),
}

struct Pixels<T>(Vec<Vec<T>>);

impl<T> Pixels<T> {
    pub fn iter_edge(&self, edge: Edge) -> Box<dyn DoubleEndedIterator<Item = &T> + '_> {
        match edge {
            Edge::Top => Box::new(self.0.first().unwrap().iter()),
            Edge::Left => Box::new(self.0.iter().map(|r| r.first().unwrap())),
            Edge::Bottom => Box::new(self.0.last().unwrap().iter()),
            Edge::Right => Box::new(self.0.iter().map(|r| r.last().unwrap())),
        }
    }

    pub fn transform(&self, transform: &Transform) -> Self
    where
        T: Copy,
    {
        let mut pixels = self.0.clone();
        if transform.rotate_left {
            pixels = (0..pixels.len())
                .map(|i| pixels.iter().map(|r| r[pixels.len() - 1 - i]).collect())
                .collect();
        }
        if transform.mirror_diagonal {
            pixels = (0..pixels.len())
                .map(|i| pixels.iter().map(|r| r[i]).collect())
                .collect();
        }
        if transform.mirror_anti_diagonal {
            pixels = (0..pixels.len())
                .map(|i| {
                    pixels
                        .iter()
                        .rev()
                        .map(|r| r[pixels.len() - 1 - i])
                        .collect()
                })
                .collect();
        }
        Pixels(pixels)
    }

    pub fn without_edges(&self) -> Self
    where
        T: Clone,
    {
        Pixels(
            self.0[1..self.0.len() - 1]
                .iter()
                .map(|r| r[1..r.len() - 1].to_vec())
                .collect(),
        )
    }
}

impl Debug for Pixels<bool> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for x in &self.0 {
            for y in x {
                f.write_str(match *y {
                    false => ".",
                    true => "#",
                })?;
            }
            f.write_str("\n")?;
        }
        Ok(())
    }
}

impl Debug for Pixels<Needle<bool>> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for x in &self.0 {
            for y in x {
                f.write_str(match *y {
                    Needle::Ignore => " ",
                    Needle::Search(false) => ".",
                    Needle::Search(true) => "#",
                })?;
            }
            f.write_str("\n")?;
        }
        Ok(())
    }
}

impl Debug for Pixels<Analysed<bool>> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for x in &self.0 {
            for y in x {
                f.write_str(match *y {
                    Analysed::Undetected(false) => ".",
                    Analysed::Undetected(true) => "#",
                    Analysed::Detected(false) => " ",
                    Analysed::Detected(true) => "O",
                })?;
            }
            f.write_str("\n")?;
        }
        Ok(())
    }
}

struct Tile {
    index: usize,
    id: u64,
    pixels: Pixels<bool>,
}

impl Debug for Tile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!(
            "({}) Tile {}:\n{:?}",
            self.index, self.id, self.pixels
        ))
    }
}

#[derive(Clone, Debug)]
struct MatchingEdge {
    pub tile: usize,
    pub edge: Edge,
    pub reversed: bool,
}

#[derive(Clone, Debug, Default)]
struct Edges {
    top: Option<MatchingEdge>,
    left: Option<MatchingEdge>,
    bottom: Option<MatchingEdge>,
    right: Option<MatchingEdge>,
}

impl Edges {
    pub fn matching_edge(&self, edge: Edge) -> &Option<MatchingEdge> {
        match edge {
            Edge::Top => &self.top,
            Edge::Left => &self.left,
            Edge::Bottom => &self.bottom,
            Edge::Right => &self.right,
        }
    }

    pub fn matching_edge_mut(&mut self, edge: Edge) -> &mut Option<MatchingEdge> {
        match edge {
            Edge::Top => &mut self.top,
            Edge::Left => &mut self.left,
            Edge::Bottom => &mut self.bottom,
            Edge::Right => &mut self.right,
        }
    }

    pub fn transform(&self, transform: &Transform) -> Self {
        let Self {
            mut top,
            mut left,
            mut bottom,
            mut right,
        } = self.clone();
        if transform.rotate_left {
            let tmp = left;
            left = top;
            top = right;
            right = bottom;
            bottom = tmp;
            left.iter_mut().for_each(|l| l.reversed ^= true);
            right.iter_mut().for_each(|r| r.reversed ^= true);
        }
        if transform.mirror_diagonal {
            mem::swap(&mut top, &mut left);
            mem::swap(&mut bottom, &mut right);
        }
        if transform.mirror_anti_diagonal {
            mem::swap(&mut top, &mut right);
            mem::swap(&mut left, &mut bottom);
            top.iter_mut().for_each(|t| t.reversed ^= true);
            left.iter_mut().for_each(|l| l.reversed ^= true);
            bottom.iter_mut().for_each(|b| b.reversed ^= true);
            right.iter_mut().for_each(|r| r.reversed ^= true);
        }
        Self {
            top,
            left,
            bottom,
            right,
        }
    }
}

#[derive(Debug)]
struct MatchedTile<'a> {
    pub tile: &'a Tile,
    pub edges: Edges,
}

impl<'a> MatchedTile<'a> {
    pub fn iter_edge(&self, edge: Edge) -> Box<dyn DoubleEndedIterator<Item = &bool> + '_> {
        self.tile.pixels.iter_edge(edge)
    }

    pub fn matching_edge(&self, edge: Edge) -> &Option<MatchingEdge> {
        self.edges.matching_edge(edge)
    }

    pub fn matching_edge_mut(&mut self, edge: Edge) -> &mut Option<MatchingEdge> {
        self.edges.matching_edge_mut(edge)
    }
}

struct TransformedTile<'a> {
    tile: &'a Tile,
    transform: Transform,
    edges: Edges,
}

impl<'a> TransformedTile<'a> {
    pub fn matching_edge(&self, edge: Edge) -> &Option<MatchingEdge> {
        self.edges.matching_edge(edge)
    }

    pub fn top_left(matched_tiles: &'a [MatchedTile]) -> Self {
        let tile = matched_tiles
            .iter()
            .find(|t| {
                Edge::iter()
                    .filter(|e| t.matching_edge(*e).is_none())
                    .count()
                    == 2
            })
            .unwrap();
        let transform = match (
            tile.matching_edge(Edge::Top).is_none(),
            tile.matching_edge(Edge::Left).is_none(),
        ) {
            (true, true) => Default::default(),
            (true, false) => Transform {
                rotate_left: true,
                mirror_diagonal: false,
                mirror_anti_diagonal: false,
            },
            (false, true) => Transform {
                rotate_left: true,
                mirror_diagonal: false,
                mirror_anti_diagonal: true,
            },
            (false, false) => Transform {
                rotate_left: false,
                mirror_diagonal: false,
                mirror_anti_diagonal: true,
            },
        };
        Self {
            tile: tile.tile,
            edges: tile.edges.transform(&transform),
            transform,
        }
    }

    pub fn below(&self, matched_tiles: &'a [MatchedTile]) -> Self {
        let bottom_edge = self.matching_edge(Edge::Bottom).as_ref().unwrap();
        let tile = &matched_tiles[bottom_edge.tile];
        let transform = match (bottom_edge.edge, bottom_edge.reversed) {
            (Edge::Top, false) => Transform {
                rotate_left: false,
                mirror_diagonal: false,
                mirror_anti_diagonal: false,
            },
            (Edge::Left, false) => Transform {
                rotate_left: false,
                mirror_diagonal: true,
                mirror_anti_diagonal: false,
            },
            (Edge::Bottom, false) => Transform {
                rotate_left: true,
                mirror_diagonal: false,
                mirror_anti_diagonal: true,
            },
            (Edge::Right, false) => Transform {
                rotate_left: true,
                mirror_diagonal: false,
                mirror_anti_diagonal: false,
            },
            (Edge::Top, true) => Transform {
                rotate_left: true,
                mirror_diagonal: true,
                mirror_anti_diagonal: false,
            },
            (Edge::Left, true) => Transform {
                rotate_left: true,
                mirror_diagonal: true,
                mirror_anti_diagonal: true,
            },
            (Edge::Bottom, true) => Transform {
                rotate_left: false,
                mirror_diagonal: true,
                mirror_anti_diagonal: true,
            },
            (Edge::Right, true) => Transform {
                rotate_left: false,
                mirror_diagonal: false,
                mirror_anti_diagonal: true,
            },
        };
        Self {
            tile: tile.tile,
            edges: tile.edges.transform(&transform),
            transform,
        }
    }

    pub fn right(&self, matched_tiles: &'a [MatchedTile]) -> Self {
        let right_edge = self.matching_edge(Edge::Right).as_ref().unwrap();
        let tile = &matched_tiles[right_edge.tile];
        let transform = match (right_edge.edge, right_edge.reversed) {
            (Edge::Top, false) => Transform {
                rotate_left: false,
                mirror_diagonal: true,
                mirror_anti_diagonal: false,
            },
            (Edge::Left, false) => Transform {
                rotate_left: false,
                mirror_diagonal: false,
                mirror_anti_diagonal: false,
            },
            (Edge::Bottom, false) => Transform {
                rotate_left: true,
                mirror_diagonal: true,
                mirror_anti_diagonal: true,
            },
            (Edge::Right, false) => Transform {
                rotate_left: true,
                mirror_diagonal: true,
                mirror_anti_diagonal: false,
            },
            (Edge::Top, true) => Transform {
                rotate_left: true,
                mirror_diagonal: false,
                mirror_anti_diagonal: false,
            },
            (Edge::Left, true) => Transform {
                rotate_left: true,
                mirror_diagonal: false,
                mirror_anti_diagonal: true,
            },
            (Edge::Bottom, true) => Transform {
                rotate_left: false,
                mirror_diagonal: false,
                mirror_anti_diagonal: true,
            },
            (Edge::Right, true) => Transform {
                rotate_left: false,
                mirror_diagonal: true,
                mirror_anti_diagonal: true,
            },
        };
        Self {
            tile: tile.tile,
            edges: tile.edges.transform(&transform),
            transform,
        }
    }
}

impl<'a> Debug for TransformedTile<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TransformedTile")
            .field("tile", &self.tile)
            .field(
                "pixels",
                &Tile {
                    index: 0,
                    id: 0,
                    pixels: self.tile.pixels.transform(&self.transform),
                },
            )
            .field("transform", &self.transform)
            .field("edges", &self.edges)
            .finish()
    }
}

fn read_input() -> anyhow::Result<Vec<Tile>> {
    Ok(parse_input(include_str!("../input/20.txt")).finish()?.1)
}

fn parse_input(input: &str) -> IResult<&str, Vec<Tile>> {
    map(
        all_consuming(many1(terminated(parse_tile, tag("\n")))),
        |mut tiles| {
            tiles
                .drain(..)
                .enumerate()
                .map(|(index, (id, pixels))| Tile { index, id, pixels })
                .collect()
        },
    )(input)
}

fn parse_tile(input: &str) -> IResult<&str, (u64, Pixels<bool>)> {
    let (input, id) = map_res(delimited(tag("Tile "), digit1, tag(":\n")), |id: &str| {
        id.parse()
    })(input)?;
    let (input, pixels) = parse_pixels(input)?;
    Ok((input, (id, pixels)))
}

fn parse_pixels(input: &str) -> IResult<&str, Pixels<bool>> {
    map(
        many1(terminated(
            many1(alt((value(false, tag(".")), value(true, tag("#"))))),
            tag("\n"),
        )),
        |p| Pixels(p),
    )(input)
}

fn read_monster() -> anyhow::Result<Pixels<Needle<bool>>> {
    Ok(parse_monster(include_str!("../input/20-monster.txt"))
        .finish()?
        .1)
}

fn parse_monster(input: &str) -> IResult<&str, Pixels<Needle<bool>>> {
    map(
        many1(terminated(
            many1(alt((
                value(Needle::Ignore, tag(" ")),
                value(Needle::Search(false), tag(".")),
                value(Needle::Search(true), tag("#")),
            ))),
            tag("\n"),
        )),
        |p| Pixels(p),
    )(input)
}

#[test]
fn test_pixels_transform() {
    let pixels = Pixels(vec![vec![0, 1], vec![2, 3]]);
    assert_eq!(
        pixels
            .transform(&Transform {
                rotate_left: true,
                ..Default::default()
            })
            .0,
        vec![vec![1, 3], vec![0, 2]]
    );
    assert_eq!(
        pixels
            .transform(&Transform {
                mirror_diagonal: true,
                ..Default::default()
            })
            .0,
        vec![vec![0, 2], vec![1, 3]]
    );
    assert_eq!(
        pixels
            .transform(&Transform {
                mirror_anti_diagonal: true,
                ..Default::default()
            })
            .0,
        vec![vec![3, 1], vec![2, 0]]
    );
}
