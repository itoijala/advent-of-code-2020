pub fn run() -> anyhow::Result<()> {
    let public_keys = read_input();
    let private_key = break_private_key(public_keys.0);
    let encryption_key = calculate_encryption_key(private_key, public_keys.1);
    println!("{}", encryption_key);
    Ok(())
}

fn break_private_key(public_key: u64) -> u64 {
    let mut s = S;
    for i in 1.. {
        s = (s * S) % M;
        if s == public_key {
            return i;
        }
    }
    unreachable!();
}

fn calculate_encryption_key(private_key: u64, other_public_key: u64) -> u64 {
    let mut e = other_public_key;
    for _ in 0..private_key {
        e = (e * other_public_key) % M;
    }
    e
}

const M: u64 = 20201227;
const S: u64 = 7;

fn read_input() -> (u64, u64) {
    let mut nums = include_str!("../input/25.txt")
        .lines()
        .map(|d| d.parse().unwrap());
    (nums.next().unwrap(), nums.next().unwrap())
}
