use std::collections::HashMap;
use std::collections::HashSet;

use crate::day_7_1;
use crate::day_7_1::Graph;

pub fn calculate(bag: &str, input: &[(String, Vec<(usize, String)>)]) -> usize {
    let mut graph = Graph::new();
    let mut igraph = Graph::new();
    for (name, followers) in input {
        let i = graph.get_or_add_node(name.as_str());
        igraph.get_or_add_node(name.as_str());
        for (value, follower) in followers {
            let j = graph.get_or_add_node(follower.as_str());
            igraph.get_or_add_node(follower.as_str());
            graph.add_follower(i, j, value);
            igraph.add_follower(j, i, ());
        }
    }
    let bag_index = graph.get_or_add_node(bag);

    enum Item {
        Child(usize),
        Parent(usize),
    }

    let mut topo_order = Vec::new();
    let mut marked = HashSet::new();
    let mut stack = Vec::new();
    for n in 0..graph.nodes.len() - 1 {
        stack.push(Item::Child(n));
        while let Some(item) = stack.pop() {
            match item {
                Item::Child(i) => {
                    if marked.contains(&i) {
                        continue;
                    }
                    stack.push(Item::Parent(i));
                    for (f, _) in &graph.nodes[i].followers {
                        stack.push(Item::Child(*f));
                    }
                }
                Item::Parent(i) => {
                    marked.insert(i);
                    topo_order.push(i);
                }
            }
        }
    }

    let mut weights = HashMap::new();
    for n in &topo_order {
        let w = graph.nodes[*n]
            .followers
            .iter()
            .map(|(f, v)| *v * (1 + weights.get(f).unwrap()))
            .sum();
        weights.insert(n, w);
    }

    *weights.get(&bag_index).unwrap()
}

pub fn run() -> anyhow::Result<()> {
    let input = day_7_1::read_input()?;
    let result = calculate("shiny gold", &input);
    println!("{}", result);
    Ok(())
}
