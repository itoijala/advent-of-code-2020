use std::collections::HashMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map_res;
use nom::multi::separated_list1;
use nom::Finish;
use nom::IResult;

pub fn calculate(nth: usize, input: &[usize]) -> usize {
    let mut ages = HashMap::new();
    let mut previous = 0;
    for t in 0..nth + 1 {
        let n = if t < input.len() {
            input[t]
        } else {
            let (t1, t2) = ages.get(&previous).unwrap();
            if let Some(t2) = t2 {
                t1 - t2
            } else {
                0
            }
        };
        let a = if let Some((t1, _)) = ages.get(&n) {
            (t, Some(*t1))
        } else {
            (t, None)
        };
        ages.insert(n, a);
        previous = n;
    }
    previous
}

fn parse_line(input: &str) -> IResult<&str, Vec<usize>> {
    separated_list1(tag(","), map_res(digit1, |n| usize::from_str(n)))(input)
}

pub fn read_input() -> anyhow::Result<Vec<usize>> {
    let f = File::open("input/15.txt")?;
    let reader = BufReader::new(f);
    let line = reader.lines().next().unwrap()?;
    let v = all_consuming(parse_line)(&line)
        .finish()
        .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?
        .1;
    Ok(v)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(2020 - 1, &input);
    println!("{}", result);
    Ok(())
}
