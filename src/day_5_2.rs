use anyhow::bail;

use crate::day_5_1;
use crate::day_5_1::Pass;

pub fn calculate(input: &[Pass]) -> anyhow::Result<usize> {
    let mut seats = input.iter().map(Pass::get_seat).collect::<Vec<_>>();
    seats.sort_unstable();
    let min = seats.iter().min().unwrap();
    let max = seats.iter().max().unwrap();
    for s in *min..*max {
        if seats.binary_search(&s).is_err() {
            return Ok(s);
        }
    }
    bail!("No seat found");
}

pub fn run() -> anyhow::Result<()> {
    let input = day_5_1::read_input()?;
    let result = calculate(&input)?;
    println!("{}", result);
    Ok(())
}
