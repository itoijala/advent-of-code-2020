use crate::day_19_1::calculate;
use crate::day_19_1::read_input;
use crate::day_19_1::Rule;

pub fn run() -> anyhow::Result<()> {
    let mut input = read_input()?;
    input
        .rules
        .insert(8, Rule::Alternatives(vec![vec![42], vec![42, 8]]));
    input
        .rules
        .insert(11, Rule::Alternatives(vec![vec![42, 31], vec![42, 11, 31]]));
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
