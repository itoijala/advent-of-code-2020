use anyhow::bail;

use crate::day_8_1;
use crate::day_8_1::Op;
use crate::day_8_1::Vm;

pub fn calculate(ops: Vec<Op>) -> anyhow::Result<isize> {
    let mut vm = Vm::new(ops);
    for i in 0..vm.ops.len() {
        vm.reset();
        vm.swap(i);
        if vm.run() {
            return Ok(vm.accumulator);
        }
        vm.swap(i);
    }
    bail!("No swap found");
}

pub fn run() -> anyhow::Result<()> {
    let input = day_8_1::read_input()?;
    let result = calculate(input)?;
    println!("{}", result);
    Ok(())
}
