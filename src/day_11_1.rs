use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::all_consuming;
use nom::combinator::value;
use nom::multi::many1;
use nom::Finish;
use nom::IResult;

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Place {
    Floor,
    Free,
    Taken,
}

impl Place {
    pub fn seated(&self) -> usize {
        match self {
            Place::Floor => 0,
            Place::Free => 0,
            Place::Taken => 1,
        }
    }
}

pub struct Board {
    places: Vec<Vec<Place>>,
}

impl Board {
    pub fn new(places: Vec<Vec<Place>>) -> Self {
        Self { places }
    }

    fn get(&self, i: isize, j: isize) -> usize {
        if i < 0
            || i >= self.places.len() as isize
            || j < 0
            || j >= self.places[i as usize].len() as isize
        {
            return 0;
        }
        self.places[i as usize][j as usize].seated()
    }

    pub fn step(&mut self) -> bool {
        let mut new_places = self.places.clone();
        let mut changed = false;
        for i in 0..new_places.len() as isize {
            let iu = i as usize;
            for j in 0..new_places[iu].len() as isize {
                let ju = j as usize;
                let n = self.get(i - 1, j - 1)
                    + self.get(i - 1, j)
                    + self.get(i - 1, j + 1)
                    + self.get(i, j - 1)
                    + self.get(i, j + 1)
                    + self.get(i + 1, j - 1)
                    + self.get(i + 1, j)
                    + self.get(i + 1, j + 1);
                new_places[iu][ju] = match self.places[iu][ju] {
                    Place::Floor => Place::Floor,
                    Place::Free => {
                        if n == 0 {
                            changed = true;
                            Place::Taken
                        } else {
                            Place::Free
                        }
                    }
                    Place::Taken => {
                        if n >= 4 {
                            changed = true;
                            Place::Free
                        } else {
                            Place::Taken
                        }
                    }
                }
            }
        }
        self.places = new_places;
        changed
    }

    pub fn run(&mut self) {
        while self.step() {}
    }
}

pub fn calculate(input: Vec<Vec<Place>>) -> usize {
    let mut board = Board::new(input);
    board.run();
    board
        .places
        .iter()
        .map(|r| r.iter().map(Place::seated).sum::<usize>())
        .sum()
}

fn parse_line(input: &str) -> IResult<&str, Vec<Place>> {
    many1(alt((
        value(Place::Floor, tag(".")),
        value(Place::Free, tag("L")),
    )))(input)
}

pub fn read_input() -> anyhow::Result<Vec<Vec<Place>>> {
    let f = File::open("input/11.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new).and_then(|l| {
                Ok(all_consuming(parse_line)(&l)
                    .finish()
                    .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?
                    .1)
            })
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(input);
    println!("{}", result);
    Ok(())
}
