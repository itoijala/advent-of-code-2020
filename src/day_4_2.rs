use std::collections::HashMap;
use std::str::FromStr;

use crate::day_4_1;

fn validate_year(entry: &HashMap<String, String>, key: &str, min: usize, max: usize) -> bool {
    let value = entry.get(key).unwrap();
    if value.len() != 4 {
        return false;
    }
    let n = usize::from_str(value).unwrap_or(0);
    min <= n && n <= max
}

fn validate_height(value: &String) -> bool {
    if value.ends_with("cm") {
        let n = usize::from_str(&value[0..value.len() - 2]).unwrap_or(0);
        150 <= n && n <= 193
    } else if value.ends_with("in") {
        let n = usize::from_str(&value[0..value.len() - 2]).unwrap_or(0);
        59 <= n && n <= 76
    } else {
        false
    }
}

fn validate_hair_colour(value: &String) -> bool {
    if value.len() != 7 || !value.starts_with("#") {
        return false;
    }
    let value = &value[1..value.len()];
    value.chars().all(|c| "0123456789abcdef".contains(c))
}

fn validate_eye_colour(value: &String) -> bool {
    value == "amb"
        || value == "blu"
        || value == "brn"
        || value == "gry"
        || value == "grn"
        || value == "hzl"
        || value == "oth"
}

fn validate_pid(value: &String) -> bool {
    if value.len() != 9 {
        return false;
    }
    value.chars().all(|c| "0123456789".contains(c))
}

pub fn validate(entry: &HashMap<String, String>) -> bool {
    day_4_1::validate(entry)
        && validate_year(entry, "byr", 1920, 2002)
        && validate_year(entry, "iyr", 2010, 2020)
        && validate_year(entry, "eyr", 2020, 2030)
        && validate_height(entry.get("hgt").unwrap())
        && validate_hair_colour(entry.get("hcl").unwrap())
        && validate_eye_colour(entry.get("ecl").unwrap())
        && validate_pid(entry.get("pid").unwrap())
}

pub fn calculate(input: &Vec<HashMap<String, String>>) -> usize {
    input.iter().filter(|e| validate(*e)).count()
}

pub fn run() -> anyhow::Result<()> {
    let input = day_4_1::read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
