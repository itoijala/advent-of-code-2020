use anyhow::anyhow;

use crate::day_1_1;

pub fn calculate(expected_sum: usize, input: &[usize]) -> Option<(usize, usize, usize, usize)> {
    for i in 0..input.len() - 1 {
        if let Some(result) = day_1_1::calculate(expected_sum - input[i], input, i) {
            return Some((input[i], result.0, result.1, input[i] * result.2));
        }
    }
    None
}

pub fn run() -> anyhow::Result<()> {
    let mut input = day_1_1::read_input()?;
    input.sort_unstable();
    let input = input;

    let result = calculate(2020, &input).ok_or(anyhow!("No result found"))?;

    println!("{} x {} x {} = {}", result.0, result.1, result.2, result.3);
    Ok(())
}
