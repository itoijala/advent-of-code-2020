use crate::day_11_1;
use crate::day_11_1::Place;

pub struct Board {
    places: Vec<Vec<Place>>,
}

impl Board {
    pub fn new(places: Vec<Vec<Place>>) -> Self {
        Self { places }
    }

    fn has_neighbour(&self, mut i: isize, mut j: isize, di: isize, dj: isize) -> usize {
        i += di;
        j += dj;
        while i >= 0
            && i < self.places.len() as isize
            && j >= 0
            && j < self.places[i as usize].len() as isize
        {
            let p = self.places[i as usize][j as usize];
            if p != Place::Floor {
                return p.seated();
            }
            i += di;
            j += dj;
        }
        0
    }

    pub fn step(&mut self) -> bool {
        let mut new_places = self.places.clone();
        let mut changed = false;
        for i in 0..new_places.len() as isize {
            let iu = i as usize;
            for j in 0..new_places[iu].len() as isize {
                let ju = j as usize;
                let n = self.has_neighbour(i, j, -1, -1)
                    + self.has_neighbour(i, j, -1, 0)
                    + self.has_neighbour(i, j, -1, 1)
                    + self.has_neighbour(i, j, 0, -1)
                    + self.has_neighbour(i, j, 0, 1)
                    + self.has_neighbour(i, j, 1, -1)
                    + self.has_neighbour(i, j, 1, 0)
                    + self.has_neighbour(i, j, 1, 1);
                new_places[iu][ju] = match self.places[iu][ju] {
                    Place::Floor => Place::Floor,
                    Place::Free => {
                        if n == 0 {
                            changed = true;
                            Place::Taken
                        } else {
                            Place::Free
                        }
                    }
                    Place::Taken => {
                        if n >= 5 {
                            changed = true;
                            Place::Free
                        } else {
                            Place::Taken
                        }
                    }
                }
            }
        }
        self.places = new_places;
        changed
    }

    pub fn run(&mut self) {
        while self.step() {}
    }
}

pub fn calculate(input: Vec<Vec<Place>>) -> usize {
    let mut board = Board::new(input);
    board.run();
    board
        .places
        .iter()
        .map(|r| r.iter().map(Place::seated).sum::<usize>())
        .sum()
}

pub fn run() -> anyhow::Result<()> {
    let input = day_11_1::read_input()?;
    let result = calculate(input);
    println!("{}", result);
    Ok(())
}
