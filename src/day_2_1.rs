use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

use nom::bytes::complete::tag;
use nom::bytes::complete::take;
use nom::character::complete::one_of;
use nom::combinator::map_res;
use nom::combinator::recognize;
use nom::multi::many1;
use nom::Finish;
use nom::IResult;

pub struct Input {
    pub password: String,
    pub letter: char,
    pub low: usize,
    pub high: usize,
}

pub fn check(input: &Input) -> bool {
    let found = input
        .password
        .chars()
        .filter(|c| *c == input.letter)
        .count();
    return input.low <= found && found <= input.high;
}

pub fn calculate<F: FnMut(&Input) -> bool>(predicate: F, input: &[Input]) -> usize {
    input
        .iter()
        .map(predicate)
        .map(|b| if b { 1 } else { 0 })
        .sum()
}

fn parse_decimal(input: &str) -> IResult<&str, usize> {
    map_res(recognize(many1(one_of("0123456789"))), |out: &str| {
        usize::from_str(out)
    })(input)
}

fn parse_line(input: &str) -> IResult<&str, Input> {
    let (input, low) = parse_decimal(input)?;
    let (input, _) = tag("-")(input)?;
    let (input, high) = parse_decimal(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, letter) = take(1usize)(input)?;
    let (input, _) = tag(": ")(input)?;

    Ok((
        input,
        Input {
            password: input.to_string(),
            letter: letter.chars().next().unwrap(),
            low,
            high,
        },
    ))
}

pub fn read_input() -> anyhow::Result<Vec<Input>> {
    let f = File::open("input/2.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new).and_then(|l| {
                Ok(parse_line(&l)
                    .finish()
                    .map(|(_, o)| o)
                    .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?)
            })
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(check, &input);
    println!("{}", result);
    Ok(())
}
