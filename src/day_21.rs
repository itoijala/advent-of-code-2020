use std::collections::HashMap;
use std::collections::HashSet;

use nom::bytes::complete::tag;
use nom::character::complete::alpha1;
use nom::combinator::all_consuming;
use nom::multi::many1;
use nom::multi::separated_list1;
use nom::sequence::delimited;
use nom::Finish;
use nom::IResult;

pub fn run() -> anyhow::Result<()> {
    let foods = read_input()?;
    let unsafe_foods = remove_safe_ingredients(&foods);
    let part1 = part1(&foods, &unsafe_foods);
    let allergens = match_ingredients_allergens(&unsafe_foods);
    let part2 = part2(&allergens);
    println!("{}", part1);
    println!("{}", part2);
    Ok(())
}

fn part1(foods: &[Food], unsafe_foods: &[Food]) -> usize {
    let all: usize = foods.iter().map(|f| f.ingredients.len()).sum();
    let except_safe: usize = unsafe_foods.iter().map(|f| f.ingredients.len()).sum();
    all - except_safe
}

fn part2(allergens: &HashMap<Allergen, Ingredient>) -> String {
    let mut allergens: Vec<_> = allergens.into_iter().collect();
    allergens.sort_unstable();
    let ingredients: Vec<_> = allergens.into_iter().map(|(_, i)| *i).collect();
    ingredients.join(",")
}

type Ingredient<'a> = &'a str;

type Allergen<'a> = &'a str;

#[derive(Clone, Debug)]
struct Food<'a> {
    ingredients: HashSet<Ingredient<'a>>,
    allergens: HashSet<Allergen<'a>>,
}

fn build_maps<'a, 'b>(
    foods: &'b [Food<'a>],
) -> (
    HashMap<Ingredient<'a>, Vec<&'b Food<'a>>>,
    HashMap<Allergen<'a>, Vec<&'b Food<'a>>>,
) {
    let mut ingredients = HashMap::new();
    let mut allergens = HashMap::new();
    for food in foods {
        for ingredient in &food.ingredients {
            ingredients
                .entry(*ingredient)
                .or_insert_with(Vec::new)
                .push(food);
        }
        for allergen in &food.allergens {
            allergens
                .entry(*allergen)
                .or_insert_with(Vec::new)
                .push(food);
        }
    }
    (ingredients, allergens)
}

fn remove_safe_ingredients<'a>(foods: &[Food<'a>]) -> Vec<Food<'a>> {
    let (ingredients, allergens) = build_maps(foods);

    let safe_ingredients: Vec<_> = ingredients
        .iter()
        .filter(|(ingredient, foods)| {
            foods.iter().all(|food| {
                food.allergens.iter().any(|allergen| {
                    allergens
                        .get(allergen)
                        .unwrap()
                        .iter()
                        .any(|f| !f.ingredients.contains(**ingredient))
                })
            })
        })
        .map(|(i, _)| *i)
        .collect();

    let mut foods: Vec<_> = foods.iter().cloned().collect();
    for food in foods.iter_mut() {
        for ingredient in &safe_ingredients {
            food.ingredients.remove(ingredient);
        }
    }
    foods
}

fn match_ingredients_allergens<'a>(foods: &[Food<'a>]) -> HashMap<Allergen<'a>, Ingredient<'a>> {
    let (_, allergens) = build_maps(foods);
    let mut a_is = HashMap::new();
    for (allergen, foods) in allergens.iter() {
        let (food, foods) = foods.split_first().unwrap();
        let mut ingredients: HashSet<_> = food.ingredients.iter().copied().collect();
        for food in foods {
            ingredients = ingredients
                .intersection(&food.ingredients.iter().copied().collect())
                .copied()
                .collect();
        }
        a_is.insert(allergen, ingredients);
    }
    let mut a_i = HashMap::new();
    while !a_is.is_empty() {
        let (&&allergen, ingredients) = a_is
            .iter()
            .find(|(_, ingredients)| ingredients.len() == 1)
            .unwrap();
        let ingredient = *ingredients.iter().next().unwrap();
        a_i.insert(allergen, ingredient);
        a_is.remove(&allergen);
        for is in a_is.values_mut() {
            is.remove(ingredient);
        }
    }
    a_i
}

fn read_input() -> anyhow::Result<Vec<Food<'static>>> {
    Ok(parse_foods(include_str!("../input/21.txt")).finish()?.1)
}

fn parse_foods(input: &str) -> IResult<&str, Vec<Food>> {
    all_consuming(many1(parse_food))(input)
}

fn parse_food(input: &str) -> IResult<&str, Food> {
    let (input, ingredients) = separated_list1(tag(" "), alpha1)(input)?;
    let (input, allergens) = delimited(
        tag(" (contains "),
        separated_list1(tag(", "), alpha1),
        tag(")\n"),
    )(input)?;
    Ok((
        input,
        Food {
            ingredients: ingredients.into_iter().collect(),
            allergens: allergens.into_iter().collect(),
        },
    ))
}
