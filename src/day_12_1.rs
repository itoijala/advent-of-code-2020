use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::ops::Add;
use std::ops::Mul;
use std::ops::Sub;
use std::str::FromStr;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map_res;
use nom::Finish;
use nom::IResult;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Op {
    North(isize),
    South(isize),
    East(isize),
    West(isize),
    Left(isize),
    Right(isize),
    Forward(isize),
}

#[derive(Clone, Copy, Debug)]
pub struct V2 {
    x: isize,
    y: isize,
}

fn sin(d: isize) -> isize {
    match d {
        0 => 0,
        1 => 1,
        2 => 0,
        3 => -1,
        _ => unreachable!(),
    }
}

fn cos(d: isize) -> isize {
    match d {
        0 => 1,
        1 => 0,
        2 => -1,
        3 => 0,
        _ => unreachable!(),
    }
}

impl V2 {
    pub fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }

    pub fn manhattan(&self) -> isize {
        self.x.abs() + self.y.abs()
    }

    pub fn turn(&self, degrees: isize) -> Self {
        let d = ((degrees / 90) % 4 + 4) % 4;
        Self {
            x: self.x * cos(d) - self.y * sin(d),
            y: self.x * sin(d) + self.y * cos(d),
        }
    }

    pub fn rotate_around(&self, point: &Self, degrees: isize) -> Self {
        (*self - *point).turn(degrees) + *point
    }
}

impl Add for V2 {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for V2 {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Mul<isize> for V2 {
    type Output = Self;

    fn mul(self, other: isize) -> Self {
        Self {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

pub fn calculate(input: &[Op]) -> isize {
    let mut pos = V2::new(0, 0);
    let mut dir = V2::new(1, 0);
    for op in input {
        match op {
            Op::North(d) => pos = pos + V2::new(0, *d),
            Op::South(d) => pos = pos + V2::new(0, -d),
            Op::East(d) => pos = pos + V2::new(*d, 0),
            Op::West(d) => pos = pos + V2::new(-d, 0),
            Op::Left(d) => dir = dir.turn(*d),
            Op::Right(d) => dir = dir.turn(-d),
            Op::Forward(d) => pos = pos + dir * *d,
        }
    }
    pos.manhattan()
}

fn parse_number(input: &str) -> IResult<&str, isize> {
    map_res(digit1, |n| isize::from_str(n))(input)
}

fn parse_op<F: Fn(isize) -> Op>(t: &str, f: F) -> impl Fn(&str) -> IResult<&str, Op> {
    let t = t.to_string();
    move |input: &str| {
        let (input, _) = tag(t.as_str())(input)?;
        let (input, n) = parse_number(input)?;
        Ok((input, f(n)))
    }
}

fn parse_line(input: &str) -> IResult<&str, Op> {
    alt((
        parse_op("N", Op::North),
        parse_op("S", Op::South),
        parse_op("E", Op::East),
        parse_op("W", Op::West),
        parse_op("L", Op::Left),
        parse_op("R", Op::Right),
        parse_op("F", Op::Forward),
    ))(input)
}

pub fn read_input() -> anyhow::Result<Vec<Op>> {
    let f = File::open("input/12.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new).and_then(|l| {
                Ok(all_consuming(parse_line)(&l)
                    .finish()
                    .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?
                    .1)
            })
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
