use crate::day_18_1::calculate;
use crate::day_18_1::read_input;
use crate::day_18_1::Operator;

pub fn run() -> anyhow::Result<()> {
    let operators = &vec![
        (
            '+',
            Operator {
                precedence: 2,
                eval: Box::new(|l, r| l + r),
            },
        ),
        (
            '*',
            Operator {
                precedence: 1,
                eval: Box::new(|l, r| l * r),
            },
        ),
    ]
    .drain(..)
    .collect();
    let input = read_input(&operators)?;
    let result = calculate(&operators, &input);
    println!("{}", result);
    Ok(())
}
