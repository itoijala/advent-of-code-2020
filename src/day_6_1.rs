use std::collections::HashSet;
use std::fs;

use nom::bytes::complete::tag;
use nom::bytes::complete::take_while1;
use nom::multi::separated_list1;
use nom::Finish;
use nom::IResult;

fn calculate_group(group: &Vec<String>) -> usize {
    let mut set = HashSet::new();
    for m in group {
        set.extend(m.chars());
    }
    set.len()
}

pub fn calculate(input: &Vec<Vec<String>>) -> usize {
    input.iter().map(|g| calculate_group(g)).sum()
}

fn parse_group(input: &str) -> IResult<&str, Vec<String>> {
    let (input, members) = separated_list1(tag("\n"), take_while1(|c| c != '\n'))(input)?;
    Ok((input, members.iter().map(|m| m.to_string()).collect()))
}

fn parse_groups(input: &str) -> IResult<&str, Vec<Vec<String>>> {
    separated_list1(tag("\n\n"), parse_group)(input)
}

pub fn read_input() -> anyhow::Result<Vec<Vec<String>>> {
    let file = fs::read_to_string("input/6.txt")?;
    let (_, groups) = parse_groups(&file)
        .finish()
        .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?;
    Ok(groups)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
