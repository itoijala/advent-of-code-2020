use std::collections::HashSet;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::value;
use nom::Finish;
use nom::IResult;

#[derive(Clone, Copy)]
pub enum Op {
    Nop(isize),
    Acc(isize),
    Jmp(isize),
}

pub struct Vm {
    pub ops: Vec<Op>,
    pub accumulator: isize,
    pub pc: isize,
}

impl Vm {
    pub fn new(ops: Vec<Op>) -> Self {
        Vm {
            ops,
            accumulator: 0,
            pc: 0,
        }
    }

    pub fn reset(&mut self) {
        self.accumulator = 0;
        self.pc = 0;
    }

    pub fn step(&mut self) {
        let op = &self.ops[self.pc as usize];
        match op {
            Op::Nop(_) => {}
            Op::Acc(d) => self.accumulator += d,
            Op::Jmp(d) => self.pc += d - 1,
        }
        self.pc += 1;
    }

    pub fn run(&mut self) -> bool {
        let mut seen_pcs: HashSet<isize> = HashSet::new();
        loop {
            self.step();
            if seen_pcs.contains(&self.pc) {
                return false;
            } else if self.pc >= self.ops.len() as isize {
                return true;
            }
            seen_pcs.insert(self.pc);
        }
    }

    pub fn swap(&mut self, i: usize) {
        match self.ops[i] {
            Op::Nop(d) => self.ops[i] = Op::Jmp(d),
            Op::Acc(_) => {}
            Op::Jmp(d) => self.ops[i] = Op::Nop(d),
        }
    }
}

pub fn calculate(ops: Vec<Op>) -> isize {
    let mut vm = Vm::new(ops);
    vm.run();
    return vm.accumulator;
}

fn parse_rest(input: &str) -> IResult<&str, isize> {
    let (input, _) = tag(" ")(input)?;
    let (input, sign) = alt((value(1, tag("+")), value(-1, tag("-"))))(input)?;
    let (input, value) = digit1(input)?;
    let value = isize::from_str(value).unwrap();
    Ok((input, sign * value))
}

fn parse_nop(input: &str) -> IResult<&str, Op> {
    let (input, _) = tag("nop")(input)?;
    let (input, d) = parse_rest(input)?;
    Ok((input, Op::Nop(d)))
}

fn parse_acc(input: &str) -> IResult<&str, Op> {
    let (input, _) = tag("acc")(input)?;
    let (input, d) = parse_rest(input)?;
    Ok((input, Op::Acc(d)))
}

fn parse_jmp(input: &str) -> IResult<&str, Op> {
    let (input, _) = tag("jmp")(input)?;
    let (input, d) = parse_rest(input)?;
    Ok((input, Op::Jmp(d)))
}

fn parse_line(input: &str) -> IResult<&str, Op> {
    alt((parse_nop, parse_acc, parse_jmp))(input)
}

pub fn read_input() -> anyhow::Result<Vec<Op>> {
    let f = File::open("input/8.txt")?;
    let reader = BufReader::new(f);
    Ok(reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new).and_then(|l| {
                Ok(all_consuming(parse_line)(&l)
                    .finish()
                    .map_err(|e| nom::error::Error::new(e.input.to_owned(), e.code))?
                    .1)
            })
        })
        .collect::<anyhow::Result<Vec<_>, _>>()?)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(input);
    println!("{}", result);
    Ok(())
}
