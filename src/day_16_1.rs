use std::str::FromStr;

use nom::bytes::complete::tag;
use nom::bytes::complete::take_until;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map;
use nom::combinator::map_res;
use nom::multi::many1;
use nom::multi::separated_list1;
use nom::Finish;
use nom::IResult;

#[derive(Debug)]
pub struct Field {
    pub name: String,
    pub ranges: Vec<(u64, u64)>,
}

impl Field {
    pub fn is_valid(&self, value: u64) -> bool {
        self.ranges.iter().any(|(l, h)| *l <= value && value <= *h)
    }
}

#[derive(Debug)]
pub struct Fields(pub Vec<Field>);

impl Fields {
    pub fn is_valid(&self, value: u64) -> bool {
        self.0.iter().any(|f| f.is_valid(value))
    }
}

pub type Ticket = Vec<u64>;

#[derive(Debug)]
pub struct Data {
    pub fields: Fields,
    pub yours: Ticket,
    pub nearby: Vec<Ticket>,
}

pub fn calculate(data: &Data) -> u64 {
    data.nearby
        .iter()
        .map(|t| t.iter())
        .flatten()
        .filter(|v| !data.fields.is_valid(**v))
        .sum()
}

fn parse_range(input: &str) -> IResult<&str, (u64, u64)> {
    let (input, low) = map_res(digit1, u64::from_str)(input)?;
    let (input, _) = tag("-")(input)?;
    let (input, high) = map_res(digit1, u64::from_str)(input)?;
    Ok((input, (low, high)))
}

fn parse_field(input: &str) -> IResult<&str, Field> {
    let (input, name) = map(take_until(":"), str::to_string)(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, ranges) = separated_list1(tag(" or "), parse_range)(input)?;
    let (input, _) = tag("\n")(input)?;
    Ok((input, Field { name, ranges }))
}

fn parse_ticket(input: &str) -> IResult<&str, Ticket> {
    let (input, ticket) = separated_list1(tag(","), map_res(digit1, u64::from_str))(input)?;
    let (input, _) = tag("\n")(input)?;
    Ok((input, ticket))
}

fn parse_data(input: &str) -> IResult<&str, Data> {
    let (input, fields) = many1(parse_field)(input)?;
    let (input, _) = tag("\nyour ticket:\n")(input)?;
    let (input, yours) = parse_ticket(input)?;
    let (input, _) = tag("\nnearby tickets:\n")(input)?;
    let (input, nearby) = many1(parse_ticket)(input)?;
    Ok((
        input,
        Data {
            fields: Fields(fields),
            yours,
            nearby,
        },
    ))
}

pub fn read_input() -> anyhow::Result<Data> {
    const INPUT: &str = include_str!("../input/16.txt");
    let (_, data) = all_consuming(parse_data)(INPUT).finish()?;
    Ok(data)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;
    let result = calculate(&input);
    println!("{}", result);
    Ok(())
}
