pub fn run() -> anyhow::Result<()> {
    let cups = read_input();
    let part_1 = part_1(100, &cups);
    println!("{}", part_1);
    let part_2 = part_2(10_000_000, 1_000_000, &cups);
    println!("{}", part_2);
    Ok(())
}

fn part_1(rounds: u64, cups: &[usize]) -> usize {
    let cups = simulate(rounds, cups);
    cups[1..].iter().fold(0, |a, d| 10 * a + (*d + 1))
}

fn part_2(rounds: u64, total_cups: usize, cups: &[usize]) -> usize {
    let mut cups = cups.to_vec();
    cups.reserve(total_cups - cups.len());
    for n in cups.len()..total_cups {
        cups.push(n);
    }

    let cups = simulate(rounds, &cups);
    (cups[1] + 1) * (cups[2] + 1)
}

fn simulate(rounds: u64, cups: &[usize]) -> Vec<usize> {
    let mut ring = vec![0; cups.len()];
    {
        let mut current = cups[0];
        for cup in &cups[1..] {
            ring[current] = *cup;
            current = *cup;
        }
        ring[current] = cups[0];
    }

    let mut current = cups[0];
    for _ in 0..rounds {
        let hand_1 = ring[current];
        let hand_2 = ring[hand_1];
        let hand_3 = ring[hand_2];
        let hand_3_next = ring[hand_3];

        let mut destination = current;
        loop {
            destination = (destination + ring.len() - 1) % ring.len();
            if destination != hand_1 && destination != hand_2 && destination != hand_3 {
                break;
            }
        }
        let destination_next = ring[destination];

        ring[current] = hand_3_next;
        ring[destination] = hand_1;
        ring[hand_3] = destination_next;

        current = ring[current];
    }

    let mut cups = Vec::with_capacity(ring.len());
    {
        let mut current = 0;
        loop {
            cups.push(current);
            current = ring[current];
            if current == 0 {
                break;
            }
        }
    }
    cups
}

fn read_input() -> Vec<usize> {
    include_str!("../input/23.txt")
        .chars()
        .filter(char::is_ascii_digit)
        .map(|c| c.to_digit(10).unwrap() as usize - 1)
        .collect()
}
