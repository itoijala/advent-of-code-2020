use std::cmp;

use crate::day_9_1;

pub fn calculate(input: &[usize]) -> Option<usize> {
    let invalid = day_9_1::calculate(input).unwrap();
    for low in 0..input.len() - 2 {
        let mut min = input[low];
        let mut max = input[low];
        let mut sum = input[low];
        for high in low + 1..input.len() - 1 {
            sum += input[high];
            min = cmp::min(min, input[high]);
            max = cmp::max(max, input[high]);
            if sum == invalid {
                return Some(min + max);
            } else if sum > invalid {
                break;
            }
        }
    }
    None
}

pub fn run() -> anyhow::Result<()> {
    let input = day_9_1::read_input()?;
    let result = calculate(&input).unwrap();
    println!("{}", result);
    Ok(())
}
