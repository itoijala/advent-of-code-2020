use crate::day_10_1;

pub fn calculate(mut input: Vec<usize>) -> usize {
    input.sort_unstable();
    let mut counts = vec![0; input.len()];
    counts[0] = 1;
    for i in 1..counts.len() {
        let mut sum = 0;
        for j in i.saturating_sub(3)..i {
            if input[i] - input[j] <= 3 {
                sum += counts[j];
            }
        }
        if input[i] <= 3 {
            sum += 1;
        }
        counts[i] = sum;
    }
    counts[counts.len() - 1]
}

pub fn run() -> anyhow::Result<()> {
    let input = day_10_1::read_input()?;
    let result = calculate(input);
    println!("{}", result);
    Ok(())
}
